package com.antonystudio.listview_load_json.response;

import okhttp3.ResponseBody;

public class FailureResponse {
    public String failureMessage;
    public ResponseBody responseBody;
    public Throwable exception;

}
