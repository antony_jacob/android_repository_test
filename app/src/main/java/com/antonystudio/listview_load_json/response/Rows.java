package com.antonystudio.listview_load_json.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Rows implements Serializable {

    @SerializedName("title")
    public String title;
    @SerializedName("description")
    public String description;
    @SerializedName("imageHref")
    public String imageHref;

    public void Rows(String title, String description, String imageHref){
        this.title = title;
        this.description = description;
        this.imageHref = imageHref;
    }

    public String getTitle() {
        return title;
    }
    public String getDescription() {
        return description;
    }
    public String getImageHref() {
        return imageHref;
    }
}
