package com.antonystudio.listview_load_json.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.ContextThemeWrapper;
import com.antonystudio.listview_load_json.R;

public class CommonUtils {

    //checking network connection
    public static boolean isNetConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }
    public static AlertDialog getAlert(Context ctx, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(ctx,
                R.style.Theme_AppCompat_Light_Dialog_Alert))
                .setMessage(message).setPositiveButton(ctx.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        return builder.create();
    }
}
