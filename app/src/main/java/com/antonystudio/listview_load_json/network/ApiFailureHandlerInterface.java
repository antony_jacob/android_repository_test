package com.antonystudio.listview_load_json.network;

import com.antonystudio.listview_load_json.response.FailureResponse;

public interface ApiFailureHandlerInterface {
    void onFailureResponse(FailureResponse response);
}
