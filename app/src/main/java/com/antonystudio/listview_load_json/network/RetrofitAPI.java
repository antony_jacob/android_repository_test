package com.antonystudio.listview_load_json.network;

import com.antonystudio.listview_load_json.response.Facts;
import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitAPI {
    @GET("/s/2iodh4vg0eortkl/facts.json")
    Call<Facts> getFactsList();
}
