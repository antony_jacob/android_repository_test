package com.antonystudio.listview_load_json.service;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.antonystudio.listview_load_json.R;
import com.antonystudio.listview_load_json.network.RetrofitAPI;
import com.antonystudio.listview_load_json.network.RetrofitClientInstance;
import com.antonystudio.listview_load_json.response.Facts;
import com.antonystudio.listview_load_json.response.FailureResponse;
import com.antonystudio.listview_load_json.utils.ServiceBus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ProficiencyTestIntentService extends IntentService {
    public static final String EXTRA_REQUEST = "extra_request";
    public static final int REQUEST_GET_LIST_DETAILS = 1;
    private ServiceBus bus;
    private Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
    private RetrofitAPI retrofitAPI = retrofit.create(RetrofitAPI.class);
    public ProficiencyTestIntentService() {
        super("ProficiencyTestIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        bus = ServiceBus.getInstance();
        retrofit = RetrofitClientInstance.getRetrofitInstance();
        retrofitAPI = retrofit.create(RetrofitAPI.class);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        assert intent != null;
        switch (intent.getIntExtra(EXTRA_REQUEST, -1)) {
            case REQUEST_GET_LIST_DETAILS:
                getFullDetails();
                break;
        }
    }

    private void getFullDetails() {
        Call<Facts> call = retrofitAPI.getFactsList();
        call.enqueue(new Callback<Facts>() {
            @Override
            public void onResponse(@NonNull Call<Facts> call, @NonNull Response<Facts> response) {
                if(response.isSuccessful()) {
                    bus.post(response.body());
                } else {
                    handleFailureResponse(null, response.errorBody());
                }
            }
            @Override
            public void onFailure(@NonNull Call<Facts> call, @NonNull Throwable t) {
                handleFailureResponse(t, null);
            }
        });
    }

    private void handleFailureResponse(Throwable throwable, ResponseBody responseBody) {
        FailureResponse failureResponse = new FailureResponse();
        failureResponse.failureMessage = getString(R.string.api_failure_message);
        failureResponse.responseBody = responseBody;
        failureResponse.exception = throwable;
        bus.post(failureResponse);
    }

}
