package com.antonystudio.listview_load_json.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.antonystudio.listview_load_json.R;
import com.antonystudio.listview_load_json.response.Rows;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FactsAdapter extends RecyclerView.Adapter<FactsAdapter.ViewHolder> {
    private List<Rows> rowsList;
    private final Context ctx;
    boolean boolTitle, boolDescription, boolImageHref;
    View item;

    public FactsAdapter(Context ctx, List<Rows> rowsList) {
        this.rowsList = rowsList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        item = LayoutInflater.from(ctx).inflate(R.layout.listview_model_item, parent, false);
        return new ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull final FactsAdapter.ViewHolder holder, int position) {
        if (rowsList.get(position).getTitle()!= null && rowsList.get(position).getTitle().length()>0) {
            holder.mTitle.setText(rowsList.get(position).getTitle());
            holder.mTitle.setVisibility(View.VISIBLE);
            boolTitle = true;
        } else {
            holder.mTitle.setVisibility(View.GONE);
            boolTitle = false;
        }
        if (rowsList.get(position).getDescription()!= null && rowsList.get(position).getDescription().length()>0) {
            holder.mDescription.setText(rowsList.get(position).getDescription());
            holder.mDescription.setVisibility(View.VISIBLE);
            boolDescription = true;
        } else {
            holder.mDescription.setVisibility(View.GONE);
            boolDescription = false;
        }
        if(rowsList.get(position).getImageHref() != null && rowsList.get(position).getImageHref().length()>0)
        {
            String imageUrl = rowsList.get(position).getImageHref().replace("http://","https://");
            Picasso.get()
                    .load(imageUrl)
                    .into(holder.mImageHrefImageVIew, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            holder.mImageHrefImageVIew.setBackgroundResource(R.drawable.error_loading);
                        }
                    });
            holder.mImageHrefImageVIew.setVisibility(View.VISIBLE);
            boolImageHref = true;
        }else {
            holder.mImageHrefImageVIew.setVisibility(View.GONE);
            boolImageHref = false;
        }
        if(!boolTitle && !boolDescription && !boolImageHref){
            holder.layout_main.setVisibility(View.GONE);
        } else if (!boolDescription && !boolImageHref) {
            holder.layout_desc_main.setVisibility(View.GONE);
        } else {
            holder.layout_main.setVisibility(View.VISIBLE);
            holder.layout_desc_main.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return rowsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        TextView mDescription;
        ImageView mImageHrefImageVIew;
        RelativeLayout layout_desc_main;
        RelativeLayout layout_main;

        public ViewHolder(View view) {
            super(view);

            mTitle = view.findViewById(R.id.textview_title);
            mDescription = view.findViewById(R.id.textview_description);
            mImageHrefImageVIew = view.findViewById(R.id.fact_imageview);
            layout_desc_main = view.findViewById(R.id.layout_desc_image);
            layout_main = view.findViewById(R.id.linear_layout_main);
        }
    }
}
