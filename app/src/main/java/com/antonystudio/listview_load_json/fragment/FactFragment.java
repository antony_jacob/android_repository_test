package com.antonystudio.listview_load_json.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.antonystudio.listview_load_json.R;
import com.antonystudio.listview_load_json.adapter.FactsAdapter;
import com.antonystudio.listview_load_json.network.ApiFailureHandlerInterface;
import com.antonystudio.listview_load_json.response.Facts;
import com.antonystudio.listview_load_json.response.FailureResponse;
import com.antonystudio.listview_load_json.service.ProficiencyTestIntentService;
import com.antonystudio.listview_load_json.utils.CommonUtils;
import com.antonystudio.listview_load_json.utils.ServiceBus;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import static android.content.Context.MODE_PRIVATE;

public class FactFragment extends Fragment implements ApiFailureHandlerInterface {
    SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressBar mProgressbar;
    RecyclerView mListView;
    AlertDialog dialog;
    protected Toolbar toolbar;
    private ServiceBus bus;
    SharedPreferences mPreference;
    Activity mActivity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_facts, container, false);
        bus = ServiceBus.getInstance();
        mActivity = getActivity();
        mSwipeRefreshLayout = view.findViewById(R.id.pull_to_refresh);
        mProgressbar = view.findViewById(R.id.progress_process);
        mListView = view.findViewById(R.id.listview_details);
        toolbar = view.findViewById(R.id.title_toolbar);
        dialog = CommonUtils.getAlert(getActivity(), "");
        mPreference = mActivity.getSharedPreferences("USER",MODE_PRIVATE);
        getData();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        return view;
    }

    //calling the service for api
    private void getData() {
        if(!CommonUtils.isNetConnected(mActivity)) {
            showBadInternetConnectionAlert(mActivity);
            return;
        }
        mProgressbar.setVisibility(View.VISIBLE);
        Intent i = new Intent(mActivity, ProficiencyTestIntentService.class);
        i.putExtra(ProficiencyTestIntentService.EXTRA_REQUEST,
                ProficiencyTestIntentService.REQUEST_GET_LIST_DETAILS);
        mActivity.startService(i);
    }

    //response from the api call
    @Subscribe
    public void factsAvailable(Facts facts) {
        mProgressbar.setVisibility(View.GONE);
        SharedPreferences.Editor editor = mPreference.edit();
        Gson gson = new Gson();
        String mgson = gson.toJson(facts);
        editor.putString("factdetails",mgson);
        editor.apply();
        toolbar.setTitle(facts.getTitle());
        if (facts.getRows().size() > 0){
            FactsAdapter adapter = new FactsAdapter(mActivity, facts.getRows());
            mListView.setHasFixedSize(true);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
            mListView.setLayoutManager(mLayoutManager);
            mListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    private void showBadInternetConnectionAlert(Activity mainActivity) {
        dialog.setMessage(mainActivity.getString(R.string.bad_internet_connection));
        if(!dialog.isShowing())
            dialog.show();
        String mJson = mPreference.getString("factdetails","");
        assert mJson != null;
        if (!mJson.equals("")){
            Gson gson = new Gson();
            Facts mFacts = gson.fromJson(mJson,Facts.class);
            factsAvailable(mFacts);
        }
    }

    @Override
    public void onFailureResponse(FailureResponse response) {

    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

}

