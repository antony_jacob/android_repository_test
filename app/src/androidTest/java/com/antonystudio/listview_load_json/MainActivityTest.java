package com.antonystudio.listview_load_json;

import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.RootMatchers;
import androidx.test.rule.ActivityTestRule;
import com.antonystudio.listview_load_json.activity.MainActivity;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> main = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testcaseForRecyclerScroll(){
        //scroll to the end of the page with position
        onView(withId(R.id.listview_details))
                .inRoot(RootMatchers.withDecorView(
                        Matchers.is(main.getActivity().getWindow().getDecorView())))
                .perform(RecyclerViewActions.scrollToPosition(1));
    }

    @Test
    public void testCaseForRecyclerItemView(){
        //viewing the items in recyclerview
        onView(withId(R.id.listview_details)).check(matches(isDisplayed()));
    }

    @Test
    public void testCaseForSwipeRefreshLayout(){
        //swipe down in swipe refresh layout
        onView(withId(R.id.pull_to_refresh)).perform(swipeDown());
    }
}
